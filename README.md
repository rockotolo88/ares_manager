## Setup

1. Clone git project
2. Create venv `python3.12 -m venv venv`
3. Activate venv `. venv/bin/activate`
3. In the BASE_DIR install `pip install -r requirements.txt`
4. Run `python manage.py migrate`
5. Click `python manage.py create_superuser`
6. Data for admin manager: `login='admin', password='admin'`
7. Use project `python manage.py runserver`