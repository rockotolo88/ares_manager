from django.contrib import admin

from .models import Company, BoardMember, Partner, Address


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'id_number',
        'name',
        'legal_form',
        'member_count',
        'partner_count',
    )
    search_fields = ('name', 'id_number')


@admin.register(BoardMember)
class BoardMemberAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'active',
        'company',
        'first_name',
        'last_name',
        'birth_date',
        'nationality',
        'title',
        'registration_date',
        'removal_date',
    )
    list_filter = (
        'active',
        'company',
        'birth_date',
        'registration_date',
        'removal_date',
    )


@admin.register(Partner)
class PartnerAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'active',
        'company',
        'type',
        'registration_date',
        'removal_date',
        'investment_amount',
        'investment_currency_name',
        'ownership_percentage',
        'paid_percentage',
        'company_name',
        'first_name',
        'last_name',
    )
    list_filter = ('active', 'company', 'registration_date', 'removal_date')


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'board_member',
        'country_code',
        'country_name',
        'region_code',
        'region_name',
        'district_code',
        'district_name',
        'municipality_code',
        'municipality_name',
        'administrative_district_code',
        'administrative_district_name',
        'municipal_district_code',
        'municipal_district_name',
        'local_part_district_code',
        'street_code',
        'local_part_district_name',
        'street_name',
        'house_number',
        'local_part_code',
        'orientation_number',
        'orientation_number_letter',
        'local_part_name',
        'address_place_code',
        'postal_code',
        'full_text_address',
        'house_number_type',
        'address_standardization',
    )
    list_filter = ('board_member', 'address_standardization')
