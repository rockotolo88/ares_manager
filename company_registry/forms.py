from django import forms


class IdNumbersForm(forms.Form):
    id_numbers = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Zadejte IČO'}))
