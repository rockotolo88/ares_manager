from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.db import IntegrityError


class Command(BaseCommand):
    help = 'Create a superuser with default credentials'

    def handle(self, *args, **options):
        try:
            User.objects.create_superuser('admin', email=None, password='admin')
            self.stdout.write(self.style.SUCCESS('Successfully created new super user'))
        except IntegrityError:
            self.stdout.write(self.style.WARNING("Superuser 'admin' already exists."))
