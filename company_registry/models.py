from django.db.models import (
    Model, CharField, IntegerField, ForeignKey, DateField, CASCADE, TextField,
    BooleanField, DecimalField, FloatField
)


class ActionMixin(Model):
    active = BooleanField(default=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if self.removal_date:
            self.active = False
        else:
            self.active = True

        super().save(*args, **kwargs)


class Company(Model):
    id_number = CharField(max_length=20, unique=True)
    name = CharField(max_length=255)
    legal_form = CharField(max_length=100)
    member_count = IntegerField(default=0)
    partner_count = IntegerField(default=0)

    def __str__(self):
        return f"{self.pk}: {self.id_number}"


class BoardMember(ActionMixin):
    company = ForeignKey(Company, related_name='board_members', on_delete=CASCADE)
    first_name = CharField(max_length=100)
    last_name = CharField(max_length=100)
    birth_date = DateField(null=True, blank=True)
    nationality = CharField(max_length=100, blank=True, null=True)
    title = CharField(max_length=100, blank=True)
    registration_date = DateField()
    removal_date = DateField(null=True, blank=True)

    class Meta:
        unique_together = (
            'company', 'first_name', 'last_name', 'birth_date', 'registration_date',
        )

    def __str__(self):
        return f"{self.pk}: {self.title} {self.first_name} {self.last_name}"


class Partner(ActionMixin):
    PARTNER_TYPE = (
        ("legal_entity", "Právnická osoba"),
        ("natural_person", "Fyzická osoba"),
    )

    company = ForeignKey(Company, related_name='partners', on_delete=CASCADE)
    type = CharField(max_length=50, choices=PARTNER_TYPE)
    registration_date = DateField()
    removal_date = DateField(null=True, blank=True)
    investment_amount = DecimalField(max_digits=15, decimal_places=2, blank=True,
                                     null=True)
    investment_currency_name = CharField(max_length=50, blank=True, null=True)
    ownership_percentage = CharField(max_length=50, blank=True, null=True)
    paid_percentage = FloatField(blank=True, null=True)
    company_name = CharField(max_length=255, blank=True, null=True,
                             help_text="For legal entity type only")
    first_name = CharField(max_length=100, blank=True, null=True,
                           help_text="For natural entity type only")
    last_name = CharField(max_length=100, blank=True, null=True,
                          help_text="For natural entity type only")

    class Meta:
        unique_together = (
            'company', 'type', 'last_name', 'registration_date', 'company_name'
        )

    def __str__(self):
        if self.first_name or self.last_name:
            return f"{self.pk}: {self.first_name, self.last_name}"

        return f"{self.pk}: {self.company_name}"


class Address(Model):
    board_member = ForeignKey(BoardMember, related_name='addresses', on_delete=CASCADE)
    country_code = CharField(max_length=3)
    country_name = CharField(max_length=32, blank=True, null=True)
    region_code = CharField(max_length=10, blank=True, null=True)
    region_name = CharField(max_length=32, blank=True, null=True)
    district_code = CharField(max_length=10, blank=True, null=True)
    district_name = CharField(max_length=32, blank=True, null=True)
    municipality_code = CharField(max_length=10, blank=True, null=True)
    municipality_name = CharField(max_length=48, blank=True, null=True)
    administrative_district_code = CharField(max_length=10, blank=True, null=True)
    administrative_district_name = CharField(max_length=32, blank=True, null=True)
    municipal_district_code = CharField(max_length=10, blank=True, null=True)
    municipal_district_name = CharField(max_length=32, blank=True, null=True)
    local_part_district_code = CharField(max_length=10, blank=True, null=True)
    street_code = CharField(max_length=10, blank=True, null=True)
    local_part_district_name = CharField(max_length=48, blank=True, null=True)
    street_name = CharField(max_length=48, blank=True, null=True)
    house_number = IntegerField(blank=True, null=True)
    local_part_code = CharField(max_length=10, blank=True, null=True)
    orientation_number = IntegerField(blank=True, null=True)
    orientation_number_letter = CharField(max_length=1, blank=True, null=True)
    local_part_name = CharField(max_length=48, blank=True, null=True)
    address_place_code = CharField(max_length=10, blank=True, null=True)
    postal_code = CharField(max_length=10, blank=True, null=True)
    full_text_address = TextField()
    house_number_type = CharField(max_length=64, blank=True, null=True)
    address_standardization = BooleanField(default=True)

    def __str__(self):
        return f"{self.pk}: {self.country_code}, {self.street_name}"
