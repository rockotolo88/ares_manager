from django.urls import path

from company_registry.views import AresDataView

urlpatterns = [
    path('', AresDataView.as_view(), name='ares-data-view'),
]
