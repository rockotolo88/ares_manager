import json
import re

import httpx
from asgiref.sync import sync_to_async
from django.db.models import Prefetch
from django.shortcuts import render
from django.views import View

from company_registry.forms import IdNumbersForm
from company_registry.models import Company, BoardMember, Address, Partner


class AresDataView(View):
    # URL for fetching data from ARES
    ares_url = "https://ares.gov.cz/ekonomicke-subjekty-v-be/rest/ekonomicke-subjekty-vr/"
    headers = {"Content-Type": "application/json; charset=utf-8"}

    async def get(self, request, *args, **kwargs):
        companies = await self.get_companies()

        # Add address to board members
        for company in companies:
            for member in company.ordered_board_members:
                member.addresses_list = await member.addresses.aget()

        context = {'companies': companies}
        return render(request, 'companies.html', context)

    async def get_companies(self):
        # Fetch companies from database
        companies_sync = await sync_to_async(list)(Company.objects.prefetch_related(
            'board_members__addresses',
            Prefetch(
                'board_members',
                queryset=BoardMember.objects.order_by('-active', 'first_name'),
                to_attr='ordered_board_members'
            ),
            Prefetch(
                'partners',
                queryset=Partner.objects.order_by('-active', 'first_name'),
                to_attr='ordered_partners'
            )
        ))
        companies = await sync_to_async(list)(companies_sync)

        for company in companies:
            for member in company.ordered_board_members:
                member.addresses_list = await member.addresses.aget()

        return companies

    async def post(self, request, *args, **kwargs):
        form = IdNumbersForm(request.POST)

        if not form.is_valid():
            return render(request, 'companies.html',
                          context={
                              'errors': {
                                  'error': 'Identification number cant be empty'
                              },
                          },
                          status=400)

        id_numbers = self.get_cleaned_list_data(form.cleaned_data['id_numbers'])

        response_data = await self.get_info_from_ares(id_numbers)

        # Use for refresh page, for test version only ;)
        companies = await self.get_companies()
        response_data['companies'] = companies

        return render(request, 'companies.html', response_data)

    async def get_info_from_ares(self, id_numbers: list) -> list:
        # Fetch data from ARES for given ID numbers
        response_data = {
            "errors": {},
            "access": {},
            "data": []
        }

        async with (httpx.AsyncClient(headers=self.headers) as client):
            for id_number in id_numbers:
                try:
                    response = await client.get(f"{self.ares_url}{id_number}")
                    response.raise_for_status()

                    data = response.json()['zaznamy'][0]
                    stat_organ = data['statutarniOrgany'][0]

                    company_name, legal_form = self.parse_company(data['obchodniJmeno'])
                    active_members, members_data = self.get_board_members(
                        stat_organ['clenoveOrganu'])
                    if partners := data.get('spolecnici'):
                        partners = partners[0]['spolecnik']
                    else:
                        partners = []

                    active_partners, partners_data = self.get_partners_data(partners)

                    company_data = {
                        "id_number": id_number,
                        "company_name": company_name,
                        "legal_form": legal_form,
                        "member_count": active_members,
                        "board_members": members_data,
                        "partner_count": active_partners,
                        "partners_data": partners_data,
                    }
                    response_data['data'].append(company_data)
                    is_created = await self.create_or_updated_company(company_data)
                    status = 'created' if is_created else 'updated'
                    response_data['access'][id_number] = (
                        f'Company with ID: {id_number} was {status}!'
                    )

                except httpx.HTTPError as e:
                    error = json.loads(e.response.text)
                    response_data['errors'][id_number] = error.get(
                        'popis', e.response.status_code)

        return response_data

    def get_board_members(self, members: list) -> tuple[int, list]:
        # Get board members data
        members_data = []
        active_members = 0
        for member in members:
            if not member.get('datumVymazu'):
                active_members += 1
                person = member.get('fyzickaOsoba', {})
                address = person.get('adresa', {})
                members_data.append({
                    "registration_date": member.get('datumZapisu'),
                    "removal_date": member.get('datumVymazu'),
                    "first_name": person.get('jmeno'),
                    "last_name": person.get('prijmeni'),
                    "birth_date": person.get('datumNarozeni'),
                    "nationality": person.get('statniObcanstvi'),
                    "title": person.get('titulPredJmenem', ''),
                    "address": self.format_address(address)
                })
        return active_members, members_data

    def format_address(self, address):
        # Format address
        return {
            'country_code': address.get('kodStatu'),
            'country_name': address.get('nazevStatu'),
            'region_code': address.get('kodKraje'),
            'region_name': address.get('nazevKraje'),
            'district_code': address.get('kodOkresu'),
            'district_name': address.get('nazevOkresu'),
            'municipality_code': address.get('kodObce'),
            'municipality_name': address.get('nazevObce'),
            'administrative_district_code': address.get('kodSpravnihoObvodu'),
            'administrative_district_name': address.get('nazevSpravnihoObvodu'),
            'municipal_district_code': address.get('kodMestskehoObvodu'),
            'municipal_district_name': address.get('nazevMestskehoObvodu'),
            'local_part_district_code': address.get('kodMestskeCastiObvodu'),
            'street_code': address.get('kodUlice'),
            'local_part_district_name': address.get('nazevMestskeCastiObvodu'),
            'street_name': address.get('nazevUlice'),
            'house_number': address.get('cisloDomovni'),
            'local_part_code': address.get('kodCastiObce'),
            'orientation_number': address.get('cisloOrientacni'),
            'orientation_number_letter': address.get('cisloOrientacniPismeno'),
            'local_part_name': address.get('nazevCastiObce'),
            'address_place_code': address.get('kodAdresnihoMista'),
            'postal_code': address.get('psc'),
            'full_text_address': address.get('textovaAdresa'),
            'house_number_type': address.get('typCisloDomovni'),
            'address_standardization': address.get('standardizaceAdresy')
        }

    def parse_company(self, company_data: list) -> (str, str):
        # Parse company name and legal form
        pattern = re.compile(
            r'(.*?)(?:,?\s*(s\.r\.o\.|a\.s\.|v\.o\.s\.|k\.s\.|družstvo|s\.p\.|p\.o\.|SE|SCE))?$'
        )

        for data in company_data:
            if data.get('datumVymazu'):
                continue

            if value := data.get('hodnota'):
                match = pattern.match(value.strip())
                if match:
                    return match.group(1).strip(), match.group(2) if match.group(
                        2) else 'Unknown'
                else:
                    return value, 'Unknown'

    def get_partners_data(self, partners: list) -> tuple[int, list]:
        # Get partners data
        if not partners:
            return 0, []

        partners_data = []
        active_partners = 0

        for partner in partners:
            if not partner.get('datumVymazu'):
                active_partners += 1

            natural_person = partner['osoba'].get('fyzickaOsoba')
            legal_entity = partner['osoba'].get('pravnickaOsoba')

            podil = partner['podil']

            if amount := podil['vklad']['hodnota']:
                amount = amount.replace(';', '.')

            if paid_percentage := podil['splaceni']['hodnota']:
                paid_percentage = paid_percentage.replace(',', '.')

            data = {
                "registration_date": partner.get('datumZapisu'),
                "removal_date": partner.get('datumVymazu'),
                "share": {
                    "investment_amount": {
                        "currency_name": podil['vklad']['typObnos'],
                        "amount": amount,
                    },
                    "ownership_percentage": podil.get('velikostPodilu', {}).get(
                        'hodnota'),
                    "paid_in_percentage": paid_percentage,
                }
            }
            if natural_person:
                data.update(
                    {
                        "type": "natural_person",
                        "birth_date": natural_person.get('datumNarozeni'),
                        "first_name": natural_person.get('jmeno'),
                        "last_name": natural_person.get('prijmeni'),
                    }
                )
            elif legal_entity:
                data.update(
                    {
                        "type": "legal_entity",
                        "company_name": legal_entity.get('obchodniJmeno'),
                    }
                )
            partners_data.append(data)
        return active_partners, partners_data

    def get_cleaned_list_data(self, id_numbers: str) -> list:
        # Clean list of ID numbers
        response_date = []

        for id_num in id_numbers.split(','):
            response_date.append(id_num.strip())

        return response_date

    async def create_or_updated_company(self, data: dict) -> bool:
        # Create or update company in database
        defaults = {
            "name": data["company_name"],
            "legal_form": data["legal_form"],
            "member_count": data["member_count"],
            "partner_count": data["partner_count"],
        }
        company, created = await Company.objects.aupdate_or_create(
            id_number=data['id_number'],
            defaults=defaults
        )
        for member_data in data["board_members"]:
            board_member, created = await BoardMember.objects.aupdate_or_create(
                company=company,
                first_name=member_data["first_name"],
                last_name=member_data["last_name"],
                birth_date=member_data["birth_date"],
                registration_date=member_data["registration_date"],
                defaults={
                    "nationality": member_data["nationality"],
                    "title": member_data["title"],
                    "removal_date": member_data["removal_date"],
                }
            )

            await Address.objects.aupdate_or_create(
                board_member=board_member,
                defaults={
                    **member_data['address']
                }

            )

        for partner_data in data["partners_data"]:
            share = partner_data.get('share', {})
            investment = share.get("investment_amount", {})

            await Partner.objects.aupdate_or_create(
                company=company,
                type=partner_data.get("type"),
                registration_date=partner_data.get("registration_date"),
                first_name=partner_data.get("first_name"),
                last_name=partner_data.get("last_name"),
                company_name=partner_data.get("company_name"),
                defaults={
                    "removal_date": partner_data.get("removal_date"),
                    "investment_amount": investment.get('amount'),
                    "investment_currency_name": investment.get('currency_name'),
                    "ownership_percentage": share.get("ownership_percentage"),
                    "paid_percentage": share.get("paid_in_percentage")
                }
            )

        return created
